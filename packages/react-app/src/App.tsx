import React, { Component } from 'react';
import './App.css';
import { MainView } from '@robo-test/components';
import { barsReduce } from '@robo-test/domain';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';


const store = createStore(combineReducers({ bar: barsReduce }), {}, applyMiddleware(logger, thunk));


class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MainView isLoading={false} itemsSource={[]} error={''}></MainView>
      </Provider>
    );
  }
}

export default App;