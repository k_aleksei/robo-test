import express from 'express';
import cors from 'cors';
import { generate as generateBar } from './Bar/bar-generator'

const app = express();
const PORT = 4200;
const yearToFail = 2010;

app.use(cors({ origin: "http://localhost:3000" }));

//get candles_by_year
app.get('/candles_by_year', (req, res) => {
    console.log(`incoming request`, req);
    const year = parseInt(req.query.year?.toString() || '', 10);
    if (year === yearToFail) {
        res.status(400);
        res.send({ status: 400, message: "For testing purposes request fails on GET 2010. Please specify another range" });
        return;
    }
    const bars = [];
    for (let index = 0; index < 10; index++) {
        bars.push(generateBar(year, index));
    }
    res.send(bars);
});


app.listen(PORT, () => {
    console.log(`Node running at ${PORT}`);
});