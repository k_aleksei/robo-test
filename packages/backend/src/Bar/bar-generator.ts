import { Bar } from '@robo-test/domain';

export const generate = (year: number, id: number) => {
    const open = id;
    const close = year - id;
    const low = id - year;
    const high = year + id;

    return new Bar(high, low, open, close);
}