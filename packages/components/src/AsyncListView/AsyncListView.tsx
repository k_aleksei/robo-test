import React, { Component } from 'react';
import Spinner from 'react-bootstrap/Spinner';
import ListGroup from 'react-bootstrap/ListGroup'
import Alert from 'react-bootstrap/Alert'
import { IItemViewModel } from '../Common/CommonViewModels';

interface IAsyncListViewState {
}

export interface IAsyncListViewProps<TItem> {
    isLoading: boolean;
    itemsSource: Array<TItem>;
    error: string;
}

export class AsyncListView<TItem extends IItemViewModel> extends Component<IAsyncListViewProps<TItem> & React.HTMLAttributes<HTMLDivElement>, IAsyncListViewState> {

    constructor(props: IAsyncListViewProps<TItem>) {
        super(props);
        this.createItemTemplate = this.createItemTemplate.bind(this);
    }

    createItemTemplate(item: TItem) {
        return <ListGroup.Item key={item.id}>{item.content}</ListGroup.Item>
    }

    shouldComponentUpdate(nextProps: IAsyncListViewProps<TItem>) {
        let shouldNotUpdate = this.props.isLoading && nextProps.isLoading;
        console.log(`current isLoading: ${this.props.isLoading} & next isLoading: ${nextProps.isLoading}. Should?:${!shouldNotUpdate}`)

        return !shouldNotUpdate;
    }

    render() {
        console.log(this.props)
        if (this.props.isLoading) {
            console.log('Loading');
            return <Spinner animation="border" variant="dark"></Spinner>;
        }

        if (this.props.error) return (
            <Alert variant="danger">
                <Alert.Heading>Something went wrong. Please find more info below:</Alert.Heading>
                <p>{this.props.error}</p>
            </Alert>
        );

        var itemTemplateCollection = this.props.itemsSource.map(this.createItemTemplate);
        return (
            <ListGroup className={this.props.className}>
                {itemTemplateCollection}
            </ListGroup>
        );
    }
}

export default AsyncListView;