import { Bar } from "@robo-test/domain";
import { IItemViewModel } from "../CommonViewModels";

export class BarListItemViewModel implements IItemViewModel {
    constructor(bar: Bar, index: number) {
        this.id = index.toString();
        this.content = `open: ${bar.open} & close: ${bar.close}`;
    }
    id: string;
    content: string;
}