export interface IItemViewModel {
    id: string,
    content: string
}