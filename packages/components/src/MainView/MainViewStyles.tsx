import { StyleSheet } from 'aphrodite';

export const styles = StyleSheet.create({
    mainPanel: {
        backgroundColor: '#f7f7f8',
        width: '100%',
        height: '500px'
    },
    mainPanel__minMaxPanel: {
        margin: '10px 0px',
        alignSelf: 'stretch',
    },
    mainPanel__listView: {
        margin: '10px 0px',
        alignSelf: 'stretch',
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        alignContent: 'stretch',
        overflowY: 'auto',
        maxHeight: '400px'
    },
    mainPanel__detailsPanel: {
        display: 'flex',
        flexDirection: 'column',
        flexWrap: 'nowrap',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
    }
});