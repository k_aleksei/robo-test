import React, { Component } from 'react';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Alert from 'react-bootstrap/Alert';
import BarForm from '../BarForm/BarForm';
import AsyncListView from '../AsyncListView/AsyncListView';
import { css } from 'aphrodite';
import { styles } from './MainViewStyles';
import { Props, IMainViewState, mainViewModelConnector } from './MainViewReduxMapping';


class MainView extends Component<Props, IMainViewState> {
    
    private getMinMaxTitle() {
        const itemsSourceReceived = this.props.itemsSource && this.props.itemsSource.length > 0;
        if (this.props.isLoading || this.props.error || !itemsSourceReceived) return undefined;
        return <Alert variant="secondary" className={css(styles.mainPanel__minMaxPanel)}>{`High: ${this.props.high} Low: ${this.props.low}`}</Alert>
    }

    private onFormSubmitted(yearFrom: number, yearTo: number): void {
        this.props.barRequestStartedAsync(yearFrom, yearTo);
    }

    render() {
        return (
            <div className={css(styles.mainPanel)}>
                <Container fluid={true}>
                    <Row>
                        <Col>
                            <BarForm onSubmit={this.onFormSubmitted.bind(this)} isLoading={this.props.isLoading}></BarForm>
                        </Col>
                        <Col className={css(styles.mainPanel__detailsPanel)}>
                            {this.getMinMaxTitle()}
                            <AsyncListView  {...this.props}  className={css(styles.mainPanel__listView)}></AsyncListView>
                        </Col>
                    </Row>
                </Container>
            </div>
        );
    }
}


export default mainViewModelConnector(MainView)