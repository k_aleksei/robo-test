import { connect, ConnectedProps } from 'react-redux';
import { IAsyncListViewProps } from '../AsyncListView/AsyncListView';
import { Bar, BarActionTypes, getBarsByYearThunk,  ApplicationState } from '@robo-test/domain';
import { ThunkDispatch } from 'redux-thunk';
import { BarListItemViewModel } from '../Common/Bar/ViewModels';

export interface IMainViewState {
}

export interface IMainViewProps extends IAsyncListViewProps<BarListItemViewModel> {
}

export interface IMainViewDispatchToProps {
    barRequestStartedAsync: (yearFrom: number, yearTo:number) => void;
}

export interface IMainViewStateToProps {
    itemsSource: BarListItemViewModel[];
    isLoading: boolean;
    error: string;
    high: number;
    low: number;
}

export const mapStateToProps = (state: ApplicationState, ownProps: IMainViewProps): IMainViewStateToProps => {
    console.log('mapStateToProps', state.bar);
    return {
        itemsSource: state.bar.bars.map((bar: Bar, index: number) => new BarListItemViewModel(bar, index)),
        error: state.bar.error,
        isLoading: state.bar.isLoading,
        high: Math.max(...state.bar.bars.map((bar: Bar)=> bar.high)),
        low: Math.min(...state.bar.bars.map((bar: Bar)=> bar.high)),
    };
};

export const mapDispatchToProps = (dispatch: ThunkDispatch<{}, {}, BarActionTypes>, ownProps: IMainViewProps): IMainViewDispatchToProps => {
    return {
        barRequestStartedAsync: async (yearFrom: number, yearTo: number) => {
            await dispatch(getBarsByYearThunk(yearFrom, yearTo));
        }
    };
};

export const mainViewModelConnector = connect(mapStateToProps, mapDispatchToProps);

type PropsFromRedux = ConnectedProps<typeof mainViewModelConnector>

export type Props = IMainViewProps & PropsFromRedux & IMainViewStateToProps & IMainViewDispatchToProps;