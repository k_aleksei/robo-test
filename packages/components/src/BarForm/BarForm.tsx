import React, { Component } from 'react';
import Button from 'react-bootstrap/Button';
import { Form } from 'react-bootstrap';

interface IBarFormState {
    from?: number;
    to?: number;
    isButtonEnabled: boolean;
}

interface IBarFormProps {
    onSubmit: (yearFrom: number, yearTo: number) => void;
    isLoading: boolean;
}



export class BarForm extends Component<IBarFormProps, IBarFormState> {
    private minYear = 2010;
    private currentYear: number;

    constructor(props: IBarFormProps) {
        super(props);

        this.currentYear = new Date().getFullYear();
        this.state = this.getInitialState();
        this.onFromYearChanged = this.onFromYearChanged.bind(this);
        this.onToYearChanged = this.onToYearChanged.bind(this);
        this.onFormSubmitted = this.onFormSubmitted.bind(this);
    }

    private getInitialState(): IBarFormState {
        return {
            from: undefined,
            to: undefined,
            isButtonEnabled: true
        }
    }

    onFromYearChanged(e: React.ChangeEvent<HTMLInputElement>) {
        const value = this.parseYear(e.target.value);
        this.setState((state, props): IBarFormState => ({
            ...state,
            from: value,
            isButtonEnabled: this.getIfButtonEnabled(value, state.to)
        }));
    }

    onToYearChanged(e: React.ChangeEvent<HTMLInputElement>) {
        const value = this.parseYear(e.target.value)
        this.setState((state, props): IBarFormState => ({
            ...state,
            to: value,
            isButtonEnabled: this.getIfButtonEnabled(state.from, value)
        }));
    }

    private parseYear(value: string): number | undefined {
        const parsed = parseInt(value, 10);
        return isNaN(parsed) ? undefined : parsed;
    }

    private getIfButtonEnabled(from: number | undefined, to: number | undefined): boolean {
        const fromNumber = from || this.minYear;
        const toNumber = to || this.currentYear;
        const fromValid = this.getIfYearInRange(fromNumber);
        const toValid = this.getIfYearInRange(toNumber);
        const inputsAreValid = fromValid && toValid && fromNumber <= toNumber;
        return (inputsAreValid) ? true : false;
    }

    private getIfYearInRange(yearToCheck: number): boolean {
        return yearToCheck >= this.minYear && yearToCheck <= this.currentYear;
    }

    onFormSubmitted(e: React.ChangeEvent<HTMLFormElement>) {
        e.preventDefault();
        this.props.onSubmit(this.state.from || this.minYear, this.state.to || this.currentYear);
    }


    render() {
        return (
            <div className="p-1">
                <Form onSubmit={this.onFormSubmitted}>
                    <Form.Group>
                        <Form.Label>From:</Form.Label>
                        <Form.Control type="text" value={String(this.state.from || '')} onChange={this.onFromYearChanged} placeholder={this.minYear.toString()} />
                        <Form.Text className="text-muted">
                            please specify FROM which year you want to see results
                            </Form.Text>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>From:</Form.Label>
                        <Form.Control type="text" value={String(this.state.to || '')} onChange={this.onToYearChanged} placeholder={this.currentYear.toString()} />
                        <Form.Text className="text-muted">
                            please specify TO which year you want to see results
                            </Form.Text>
                    </Form.Group>
                    <Button type='submit' variant="primary" className="mr-1" disabled={this.props.isLoading || !this.state.isButtonEnabled}>{this.props.isLoading ? 'Loading...' : 'Load'}</Button>
                </Form>
            </div>
        );
    }
}

export default BarForm;