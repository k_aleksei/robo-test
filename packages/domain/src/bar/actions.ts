import { ThunkAction, ThunkDispatch } from "redux-thunk";
import axios, { AxiosResponse } from 'axios';
import { Bar } from "..";
import {
    GET_BARS_BY_YEAR_STARTED,
    GET_BARS_BY_YEAR_SUCCEEDED,
    GET_BARS_BY_YEAR_FAILED,
    BarActionTypes
} from './bar-types';
import { BarsByYearRequestExecutor } from "./barsByYearRequestExecutor";



export function getBarsByYearStarted(yearFrom: number, yearTo: number): BarActionTypes {
    return {
        type: GET_BARS_BY_YEAR_STARTED,
        yearFrom,
        yearTo
    }
}

export function getBarsByYearSucceeded(bars: Array<Bar>): BarActionTypes {
    return {
        type: GET_BARS_BY_YEAR_SUCCEEDED,
        bars
    }
}

export function getBarsByYearFailed(error: string): BarActionTypes {
    return {
        type: GET_BARS_BY_YEAR_FAILED,
        error: error
    }
}

export const getBarsByYearThunk = (yearFrom: number, yearTo: number): ThunkAction<Promise<void>, {}, {}, BarActionTypes> => {
    return async (dispatch: ThunkDispatch<{}, {}, BarActionTypes>): Promise<void> => {
        return new Promise<void>((resolve, reject) => {
            dispatch(getBarsByYearStarted(yearFrom, yearTo));
            new BarsByYearRequestExecutor(yearFrom, yearTo, (bars)=> {dispatch(getBarsByYearSucceeded(bars)); resolve()}, (error)=>{dispatch(getBarsByYearFailed(error)); reject()}).start();  
        });
    }
}