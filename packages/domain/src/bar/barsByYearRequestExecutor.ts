import axios, { AxiosResponse } from 'axios';
import { Bar } from '..';

export class BarsByYearRequestExecutor {
    private yearsToRequest: Array<() => Promise<AxiosResponse<Bar[]>>> = [];
    private requestedYears: Array<Promise<AxiosResponse<Bar[]>>> = [];
    private requestsInProgress: number = 0;
    private bars: Bar[] = [];

    constructor(yearFrom: number, yearTo: number, private resolve: (bars: Bar[]) => void, private reject: (error: string) => void) {
        for (let year = yearFrom; year <= yearTo; year++) {
            this.yearsToRequest.push(() => axios.get<Array<Bar>>(`http://localhost:4200/candles_by_year?year=${year}`));
        }
    }

    public start() {
        this.requestNextYearIfCan();
    }

    private requestNextYearIfCan() {
        if (this.requestsInProgress >= 2) return;
        const newRequestFactory = this.yearsToRequest.pop();
        if (newRequestFactory) {
            const newRunningRequest = newRequestFactory();
            this.requestedYears.push(newRunningRequest)
            this.requestsInProgress++;
            newRunningRequest
                .then((res: AxiosResponse<Bar[]>) => this.completeOrProceed(res.data))
                .catch(res => this.fail(res.response?.data));
        }
        this.requestNextYearIfCan();
    }

    private completeOrProceed(bars: Bar[]) {
        this.requestsInProgress--;
        this.bars.push(...bars);
        if (this.yearsToRequest.length > 0) { this.requestNextYearIfCan(); return; }
        this.resolve(this.bars);
    }

    private fail(error: {status:any, message:string}) {
        this.reject(error.message);
    }
}