import {
    GET_BARS_BY_YEAR_STARTED,
    GET_BARS_BY_YEAR_FAILED,
    GET_BARS_BY_YEAR_SUCCEEDED,
    BarActionTypes,
    IBarRepository
} from './bar-types'

const initialState: IBarRepository = {
    isLoading: false,
    bars: [],
    error: ''
};

export function barsReduce(state: IBarRepository = initialState, action: BarActionTypes): IBarRepository {
    console.log('barsReduce',state);
    switch (action.type) {
        case GET_BARS_BY_YEAR_STARTED:
            return {
                ...state,
                isLoading: true
            };
        case GET_BARS_BY_YEAR_SUCCEEDED:
            return {
                ...state,
                bars: action.bars,
                isLoading: false,
                error: ''
            };
        case GET_BARS_BY_YEAR_FAILED:
            return {
                ...state,
                error: action.error,
                isLoading: false
            };
        default:
            return state;
    }
}