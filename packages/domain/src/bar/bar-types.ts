import Bar from "./bar";

export const GET_BARS_BY_YEAR_STARTED = "GET_BARS_BY_YEAR_STARTED";
export const GET_BARS_BY_YEAR_SUCCEEDED = "GET_BARS_BY_YEAR_SUCCEEDED";
export const GET_BARS_BY_YEAR_FAILED = "GET_BARS_BY_YEAR_FAILED";

interface GetBarsByYearAction {
  type: typeof GET_BARS_BY_YEAR_STARTED;
  yearFrom: number;
  yearTo: number;
}

interface GetBarsByYearSucceededAction {
  type: typeof GET_BARS_BY_YEAR_SUCCEEDED;
  bars: Array<Bar>;
}

interface GetBarsByYearFailedAction {
  type: typeof GET_BARS_BY_YEAR_FAILED;
  error: string;
}

export interface IBarRepository {
  isLoading: boolean;
  bars: Array<Bar>;
  error: string;
}


export type BarActionTypes = GetBarsByYearAction | GetBarsByYearSucceededAction | GetBarsByYearFailedAction;