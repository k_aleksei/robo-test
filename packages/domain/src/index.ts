import { IBarRepository } from './bar/bar-types';

// The top-level state object
export interface ApplicationState {
  bar: IBarRepository
}


export * from "./bar/bar";
export * from './bar/bar-types'
export * from './bar/actions'
export { barsReduce } from './bar/reducers'